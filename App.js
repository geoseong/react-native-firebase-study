/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';

import firebase from 'react-native-firebase';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  notificationListener;

  /* Add this Block (BEGIN) */
  componentDidMount() {
    firebase.messaging().hasPermission().then(enabled => {
      if (enabled) {
        firebase.messaging().getToken().then(token => {
          console.log("[Token] LOG: ", token);
        })
        // user has permissions
      } else {
        firebase.messaging().requestPermission()
          .then(() => {
            alert("User Now Has Permission")
          })
          .catch(error => {
            alert("Error", error)
            // User has rejected permissions  
          });
      }
    });

    this.notificationListener = firebase.notifications().onNotification((notification) => {
      console.log("[Noti]notification: ", notification);
      /* notification constructure
        { 
          _android: { 
            _notification: [Circular],
            _actions: [],
            _people: [],
            _smallIcon: { icon: 'ic_launcher' } 
          },
          _ios: { 
            _notification: [Circular],
            _alertAction: undefined,
            _attachments: [],
            _badge: undefined,
            _category: '',
            _hasAction: undefined,
            _launchImage: '',
            _threadIdentifier: '' 
          },
          _body: 'myname',
          _data: { 
            'google.c.a.ts': '1536079080',
            'google.c.a.c_id': '8018338889909034580',
            worker_id: 'worker',
            'google.c.a.udt': '0',
            'google.c.a.c_l': 'lbl',
            'gcm.n.e': '1',
            'google.c.a.e': '1',
            proj_id: 'proj-48' 
          },
          _notificationId: '83687E73-5408-46FC-B3B1-BBEA5C924AC7',
          _sound: undefined,
          _subtitle: undefined,
          _title: 'parameter 나갑니다잉' 
        }
      */
      // Process your notification as required
      const {
        body,
        data,
        notificationId,
        sound,
        subtitle,
        title
      } = notification;
      console.log("[Noti]LOG: ", title, body, JSON.stringify(data));
      console.log("[Noti]Platform: ", Platform);

      /* Displaying Notification */
      const pushnotification = new firebase.notifications.Notification()
        .setNotificationId('notificationId')
        .setTitle(title)
        .setBody(body)
        .setData({
          worker: data.worker_id,
          project: data.proj_id,
        });
      if (Platform.OS === 'ios') {
        pushnotification.ios.setBadge(2);
      } else {
        pushnotification
          .android.setChannelId('channelId')
          .android.setSmallIcon('ic_launcher');
      }
      console.log("[Noti]pushnotification:", pushnotification);
      firebase.notifications().displayNotification(pushnotification);
    }); // end onNotification()
  }

  componentWillUnmount() {
    this.notificationListener();
  }
  /* Add this Block (END) */

  /* Firebase token:
    2018/09/05 01am-c2bLAVItA5Y:APA91bEYeyAyhGIS_CzfyK2-I6j0nvXHrxWxASvcLvNurDz57O_-kWJ-L3q7e5nH5GVNoQYTy1fO7JZF--BIuvANVhRJA39i2Gja5b_gm1ZssiJ2Hisq-oMNAfWprW_fe_ydMF1svuTs

  */

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>
          Welcome to React Native!
        </Text>
        <Text style={styles.instructions}>
          To get started, edit App.js
        </Text>
        <Text style={styles.instructions}>
          {instructions}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
