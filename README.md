## React-Native-Firebase Study

- This is toy project referring to [react-native-firebase](https://github.com/invertase/react-native-firebase)
- What I tested is...
  - Displaying Notification
  - Receiving Cloud Messaging
- I tested on Android / iOS device and it's working well.
- Another reference link
  - [React Native Firebase Documentation](https://rnfirebase.io/docs/)
  - [React Native Firebase Cloud Messaging — iOS](https://medium.com/@paul.allies/react-native-firebase-cloud-messaging-ios-97b59e5f28ec)