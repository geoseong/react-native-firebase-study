import { AppRegistry } from 'react-native';
import App from './App';
import bgMessaging from './bgMessaging'; // <-- Import the file you created in (2)

AppRegistry.registerComponent('myfirebasenoti', () => App);
// New task registration
AppRegistry.registerHeadlessTask('RNFirebaseBackgroundMessage', () => bgMessaging); // <-- Add this line